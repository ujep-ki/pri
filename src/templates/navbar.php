<?php
    $inNew = str_starts_with($_SERVER['REQUEST_URI'], '/new') ? 'disabled' : '';
?>

<nav class="navbar navbar-expand-lg bg-body-tertiary bg-dark" data-bs-theme="dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Fakultoseznam</a>

        <a class="btn btn-outline-success <?= $inNew ?>" href="/new">Přidat fakultu</a>
    </div>
</nav>
<br />
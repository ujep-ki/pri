<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <xsl:apply-templates select="fakulta"/>
    </xsl:template>

    <xsl:template match="fakulta">
        <xsl:for-each select="katedry/katedra">
            <xsl:sort select="@zkratka_katedry"/>
            <xsl:apply-templates select="."/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="vedoucí">
        <article class="row">
            <h3>Vedoucí Katedry:</h3>
            <section class="form-label">
                <label style="font-weight: bolder">Jméno:&#160;</label>
                <label><xsl:value-of select="jméno"/></label>
            </section>
            <section class="form-label">
                <label style="font-weight: bolder">Telefon:&#160;</label>
                <label><xsl:value-of select="telefon"/></label>
            </section>
            <section class="form-label">
                <label style="font-weight: bolder">Email:&#160;</label>
                <label><xsl:value-of select="email"/></label>
            </section>
        </article>
    </xsl:template>

    <xsl:template match="zaměstnanec">
        <article class="row">
            <section class="form-label">
                <label style="font-weight: bolder">Jméno:&#160;</label>
                <label><xsl:value-of select="jméno"/></label>
            </section>
            <section class="form-label">
                <label style="font-weight: bolder">Telefon:&#160;</label>
                <label><xsl:value-of select="telefon"/></label>
            </section>
            <section class="form-label">
                <label style="font-weight: bolder">Email:&#160;</label>
                <label><xsl:value-of select="email"/></label>
            </section>
        </article>
    </xsl:template>

    <xsl:template match="předmět">
        <article class="row">
            <section class="form-label">
                <label style="font-weight: bolder">
                    <xsl:value-of select="@zkratka"/>:&#160;
                </label>
                <label>
                    <xsl:value-of select="název"/>
                </label>
            </section>

            <section class="form-label">
                <label style="font-weight: bolder">
                    Typ:&#160;
                </label>
                <label>
                    <xsl:value-of select="@typ"/>
                </label>
            </section>

            <section class="form-label">
                <label style="font-weight: bolder">
                    Popis:&#160;
                </label>
                <p>
                    <xsl:value-of select="popis"/>
                </p>
            </section>
        </article>
    </xsl:template>

    <xsl:template match="katedra">
        <article class="w3-card-4 w3-bottombar w3-border-teal w3-padding-16">
            <h2 class="w3-container w3-serif w3-text-teal">
                <xsl:value-of select="@zkratka_katedry"/> -
                <xsl:value-of select="@název"/>
            </h2>
            <section class="w3-container">
                <xsl:for-each select="vedoucí">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
            </section>

            <hr size="3" />

            <section class="row">
                <article class="col-12 col-md-6">
                    <h3>Zaměstnanci</h3>
                    <xsl:for-each select="zaměstnanci/zaměstnanec">
                        <xsl:variable name="i" select="position() - 1"/>
                        <xsl:if test="$i != 0">
                            <hr/>
                        </xsl:if>
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </article>
                <article class="d-block d-md-none">
                    <hr size="2" />
                </article>
                <article class="col-12 col-md-6">
                    <h3>Předmety</h3>
                    <section>
                        <xsl:for-each select="předměty/předmět">
                            <xsl:variable name="i" select="position() - 1"/>
                            <xsl:if test="$i != 0">
                                <hr/>
                            </xsl:if>
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </section>
                </article>
            </section>
        </article>
    </xsl:template>

</xsl:transform>
<article>
    <h2><a href="/new/department/?idx=<?=$idx?>">Katedra: <?= $department->getName(); ?> (<?= $department->getShortcut(); ?>)</a></h2>
    <article class="row">
        <h3>Vedoucí Katedry:</h3>
        <label class="form-label">Jméno: <?= $department->getSupervisor()->getName(); ?></label>
        <label class="form-label">Telefon: <?= $department->getSupervisor()->getPhone(); ?></label>
        <label class="form-label">Email: <?= $department->getSupervisor()->getEmail(); ?></label>
    </article>
</article>
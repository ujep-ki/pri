<?php

class Subject
{
    private string $name;
    private string $shortcut;
    private string $type;
    private string $description;

    /**
     * @param string $name
     * @param string $shortcut
     * @param string $type
     * @param string $description
     */
    public function __construct(string $name, string $shortcut, string $type, string $description)
    {
        $this->name = $name;
        $this->shortcut = $shortcut;
        $this->type = $type;
        $this->description = $description;
    }

    public static function fromXml(SimpleXMLElement $element): Subject
    {
        return new Subject(
            (string) $element->xpath("./název")[0],
            Utils::xml_attribute($element, 'zkratka'),
            Utils::xml_attribute($element, 'typ'),
            (string) $element->xpath("./popis")[0],
        );
    }

    public static function fromPost(array $data)
    {
        return new Subject(
            $data['name'],
            $data['shortcut'],
            $data['type'],
            $data['description'],
        );
    }

    public function toXml(SimpleXMLElement $parent): SimpleXMLElement
    {
        $node = $parent->addChild('předmět');

        $node->addAttribute('zkratka', $this->shortcut);
        $node->addAttribute('typ', $this->type);

        $node->addChild('název', $this->name);
        $node->addChild('popis', $this->description);

        return $node;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortcut(): string
    {
        return $this->shortcut;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function saveDB(mysqli $connection, int $departmentId)
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "INSERT INTO subjects(name, shortcut, type, description, department_id) VALUES (?, ?, ?, ?, ?)");
        mysqli_stmt_bind_param($prepare, 'ssssi', $this->name, $this->shortcut, $this->type, $this->description, $departmentId);
        mysqli_stmt_execute($prepare);
    }

    public static function getFromDB(mysqli $connection, int $subjectId) : Subject
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "SELECT name, shortcut, type, description FROM subjects where id=?");
        mysqli_stmt_bind_param($prepare, 'i', $subjectId);
        mysqli_stmt_execute($prepare);
        $subjectData = mysqli_fetch_assoc(mysqli_stmt_get_result($prepare));

        return new Subject(
            $subjectData['name'],
            $subjectData['shortcut'],
            $subjectData['type'],
            $subjectData['description'],
        );
    }

    public static function getSubjectsByDepartmentIdDB(mysqli $connection, int $departmentId) : array
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "SELECT id FROM subjects where department_id=?");
        mysqli_stmt_bind_param($prepare, 'i', $departmentId);
        mysqli_stmt_execute($prepare);
        $subjectIds = mysqli_fetch_all(mysqli_stmt_get_result($prepare), MYSQLI_ASSOC);

        $subjects = [];

        foreach ($subjectIds as $row) {
            $subjects[] = Subject::getFromDB($connection, $row['id']);
        }

        return $subjects;
    }
}
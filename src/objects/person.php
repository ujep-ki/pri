<?php

abstract class Person
{
    protected string $name;
    protected string $phone;
    protected string $email;

    /**
     * @param string $name
     * @param string $phone
     * @param string $email
     */
    public function __construct(string $name, string $phone, string $email)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    public abstract function saveDB(mysqli $connection, int $parentId);
}
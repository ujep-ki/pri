<?php
include_once 'person.php';

class DepartmentSupervisor extends Person
{
    public static function fromXml(SimpleXMLElement $node)
    {
        return new DepartmentSupervisor(
            (string) $node->xpath("./jméno")[0],
            (string) $node->xpath("./telefon")[0],
            (string) $node->xpath("./email")[0],
        );
    }

    public function saveDB(mysqli $connection, int $_)
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "INSERT INTO persons(name, phone, email) VALUES (?, ?, ?)");
        mysqli_stmt_bind_param($prepare, 'sss', $this->name, $this->phone, $this->email);
        mysqli_stmt_execute($prepare);

        $data = mysqli_fetch_all(mysqli_query($connection, "SELECT LAST_INSERT_ID() as `id`;"), MYSQLI_ASSOC);

        return $data[0]['id'];
    }

    public static function getOneByIdDB(mysqli $connection, int $personId) : DepartmentSupervisor
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "SELECT name, phone, email FROM persons WHERE id=?");
        mysqli_stmt_bind_param($prepare, 'i', $personId);
        mysqli_stmt_execute($prepare);
        $personData = mysqli_fetch_assoc(mysqli_stmt_get_result($prepare));

        return new DepartmentSupervisor(
            $personData['name'],
            $personData['phone'],
            $personData['email'],
        );
    }

    public function toXml(SimpleXMLElement $parent): SimpleXMLElement
    {
        $node = $parent->addChild('vedoucí');
        $node->addChild('jméno', $this->name);
        $node->addChild('telefon', $this->phone);
        $node->addChild('email', $this->email);

        return $node;
    }
}
<?php
include_once '/var/www/objects/department_supervisor.php';
include_once '/var/www/objects/employee.php';
include_once '/var/www/objects/subject.php';

class Department
{
    private string $name;
    private string $shortcut;
    private DepartmentSupervisor $supervisor;
    private array $employees;
    private array $subjects;

    /**
     * @param string $name
     * @param string $shortcut
     * @param DepartmentSupervisor $supervisor
     * @param array $employees
     * @param array $subjects
     */
    public function __construct(string $name, string $shortcut, DepartmentSupervisor $supervisor, array $employees = [], array $subjects = [])
    {
        $this->name = $name;
        $this->shortcut = $shortcut;
        $this->supervisor = $supervisor;
        $this->employees = $employees;
        $this->subjects = $subjects;
    }

    static function fromPostData(array $data) : Department
    {
        $supervisor = new DepartmentSupervisor(
            $data['supervisor-name'],
            $data['supervisor-phone'],
            $data['supervisor-email']
        );

        return new Department($data['name'], $data['shortcut'], $supervisor);
    }

    public static function fromXml(SimpleXMLElement $department): Department
    {
        $employees = [];
        $subjects = [];

        foreach ($department->xpath("./zaměstnanci/zaměstnanec") as $employee) {
            $employees[] = Employee::fromXml($employee);
        }

        foreach ($department->xpath("./předměty/předmět") as $subject) {
            $subjects[] = Subject::fromXml($subject);
        }

        return new Department(
            Utils::xml_attribute($department, 'název'),
            Utils::xml_attribute($department, 'zkratka_katedry'),
            DepartmentSupervisor::fromXml($department->xpath("./vedoucí")[0]),
            $employees,
            $subjects
        );
    }

    public function toXml(SimpleXMLElement $parent): SimpleXMLElement
    {
        $node = $parent->addChild('katedra');
        $node->addAttribute('zkratka_katedry', $this->shortcut);
        $node->addAttribute('název', $this->name);

        $this->supervisor->toXml($node);

        $employeesNode = $node->addChild('zaměstnanci');
        $subjectsNode = $node->addChild('předměty');

        foreach ($this->employees as $employee) {
            $employee->toXml($employeesNode);
        }

        foreach ($this->subjects as $subject) {
            $subject->toXml($subjectsNode);
        }

        return $node;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortcut(): string
    {
        return $this->shortcut;
    }

    /**
     * @return DepartmentSupervisor
     */
    public function getSupervisor(): DepartmentSupervisor
    {
        return $this->supervisor;
    }

    /**
     * @return array
     */
    public function &getEmployees(): array
    {
        return $this->employees;
    }

    /**
     * @return array
     */
    public function &getSubjects(): array
    {
        return $this->subjects;
    }

    public function saveToDB(mysqli $connection, int $facultyId) {
        $supervisorId = $this->supervisor->saveDB($connection, (int) null);

        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "INSERT INTO departments(name, shortcut, faculty_id, supervisor_id) VALUES (?, ?, ?, ?)");
        mysqli_stmt_bind_param($prepare, 'ssii', $this->name, $this->shortcut, $facultyId, $supervisorId);
        mysqli_stmt_execute($prepare);

        $data = mysqli_fetch_all(mysqli_query($connection, "SELECT LAST_INSERT_ID() as `id`;"), MYSQLI_ASSOC);

        $departmentId = $data[0]['id'];

        foreach ($this->employees as $employee) {
            $employee->saveDB($connection, $departmentId);
        }

        foreach ($this->subjects as $subject) {
            $subject->saveDB($connection, $departmentId);
        }

        return $departmentId;
    }

    public static function getOneByIdDB(mysqli $connection, int $departmentId) {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "SELECT * FROM departments where id=?");
        mysqli_stmt_bind_param($prepare, 'i', $departmentId);
        mysqli_stmt_execute($prepare);
        $departmentData = mysqli_fetch_assoc(mysqli_stmt_get_result($prepare));

        return new Department(
            $departmentData['name'],
            $departmentData['shortcut'],
            DepartmentSupervisor::getOneByIdDB($connection, $departmentData['supervisor_id']),
            Employee::getEmploysByDepartmentFromDB($connection, $departmentId),
            Subject::getSubjectsByDepartmentIdDB($connection, $departmentId)
        );
    }

    public static function getAllByFacultyIdDB(mysqli $connection, int $facultyId): array {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "SELECT id FROM departments where faculty_id=?");
        mysqli_stmt_bind_param($prepare, 'i', $facultyId);
        mysqli_stmt_execute($prepare);
        $departmentIds = mysqli_fetch_all(mysqli_stmt_get_result($prepare), MYSQLI_ASSOC);

        $departments = [];

        foreach ($departmentIds as $row) {
            $departments[] = Department::getOneByIdDB($connection, $row['id']);
        }

        return $departments;
    }
}
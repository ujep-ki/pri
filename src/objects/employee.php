<?php
include_once 'person.php';

class Employee extends Person
{

    public static function fromPost(array $data): Employee
    {
        return new Employee(
          $data['name'],
          $data['phone'],
          $data['email'],
        );
    }

    public function toXml(SimpleXMLElement $parent): void
    {
        $dom = $parent->addChild('zaměstnanec');
        $dom->addChild('jméno', $this->name);
        $dom->addChild('telefon', $this->phone);
        $dom->addChild('email', $this->email);
    }

    public static function fromXml(SimpleXMLElement $element): Employee
    {
        return new Employee(
            (string) $element->xpath("./jméno")[0],
            (string) $element->xpath("./telefon")[0],
            (string) $element->xpath("./email")[0]
        );
    }

    public function saveDB(mysqli $connection, int $departmentId)
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "INSERT INTO persons(name, phone, email) VALUES (?, ?, ?)");
        mysqli_stmt_bind_param($prepare, 'sss', $this->name, $this->phone, $this->email);
        mysqli_stmt_execute($prepare);

        $data = mysqli_fetch_all(mysqli_query($connection, "SELECT LAST_INSERT_ID() as `id`;"), MYSQLI_ASSOC);

        $personId = $data[0]['id'];

        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "INSERT INTO employees(department_id, person_id) VALUES (?, ?)");
        mysqli_stmt_bind_param($prepare, 'ii', $departmentId, $personId);
        mysqli_stmt_execute($prepare);

        return $personId;
    }

    public static function getFromDB(mysqli $connection, int $personId): Employee
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "SELECT name, phone, email FROM persons WHERE id=?");
        mysqli_stmt_bind_param($prepare, 'i', $personId);
        mysqli_stmt_execute($prepare);
        $personData = mysqli_fetch_assoc(mysqli_stmt_get_result($prepare));

        return new Employee(
            $personData['name'],
            $personData['phone'],
            $personData['email'],
        );
    }

    public static function getEmploysByDepartmentFromDB(mysqli $connection, int $personId): array
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "SELECT person_id FROM employees WHERE department_id=?");
        mysqli_stmt_bind_param($prepare, 'i', $personId);
        mysqli_stmt_execute($prepare);
        $employeeIds = mysqli_fetch_all(mysqli_stmt_get_result($prepare), MYSQLI_ASSOC);

        $employees = [];

        foreach ($employeeIds as $row) {
            $employees[] = Employee::getFromDB($connection, $row['person_id']);
        }

        return $employees;
    }
}
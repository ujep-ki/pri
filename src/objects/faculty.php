<?php
include_once '/var/www/objects/department.php';
include_once '/var/www/utils.php';

final class Faculty {
    private string $name;
    private string $dean;
    private array $departments;

    /**
     * @param string $name
     * @param string $dean
     */
    public function __construct(string $name, string $dean, array $departments = [])
    {
        $this->name = $name;
        $this->dean = $dean;
        $this->departments = $departments;
    }

    public static function fromPostData(array $data) : Faculty {
        return new Faculty($data['faculty-name'], $data['dean-name']);
    }

    public static function fromXml(SimpleXMLElement $facultyElement): Faculty {
        $departments = [];

        foreach ($facultyElement->xpath("./katedry/katedra") as $department) {
            $departments[] = Department::fromXml($department);
        }

        return new Faculty(
            Utils::xml_attribute($facultyElement, 'název'),
            Utils::xml_attribute($facultyElement, 'děkan'),
            $departments
        );
    }

    public function toXml(): SimpleXMLElement
    {
        $node = new SimpleXMLElement('<fakulta></fakulta>');
        $departmentsNode = $node->addChild('katedry');

        $node->addAttribute('děkan', $this->dean);
        $node->addAttribute('název', $this->name);

        foreach ($this->departments as $department) {
            $department->toXml($departmentsNode);
        }

        return $node;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDean(): string
    {
        return $this->dean;
    }

    /**
     * @return array
     */
    public function &getDepartments(): array
    {
        return $this->departments;
    }

    public function saveToDB(mysqli $connection): int {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "INSERT INTO faculties(name, dean_name) VALUES (?, ?)");
        mysqli_stmt_bind_param($prepare, 'ss', $this->name, $this->dean);
        mysqli_stmt_execute($prepare);

        $data = mysqli_fetch_all(mysqli_query($connection, "SELECT LAST_INSERT_ID() as `id`;"), MYSQLI_ASSOC);

        $facultyId = $data[0]['id'];

        foreach ($this->departments as $department) {
            $department->saveToDB($connection, $facultyId);
        }

        return $facultyId;
    }

    public static function getOneByIdDB(mysqli $connection, int $facultyId): Faculty
    {
        $prepare = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($prepare, "SELECT * from faculties where id=?");
        mysqli_stmt_bind_param($prepare, 'i', $facultyId);
        mysqli_stmt_execute($prepare);
        $facultyData = mysqli_fetch_assoc(mysqli_stmt_get_result($prepare));

        return new Faculty(
            $facultyData['name'],
            $facultyData['dean_name'],
            Department::getAllByFacultyIdDB($connection, $facultyId)
        );
    }
}
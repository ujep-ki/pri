<?php
include_once '../../../objects/faculty.php';
include_once '../../../objects/department.php';

session_start();

/** @var Faculty $faculty */
$faculty = $_SESSION['new'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $department = Department::fromPostData($_POST);

    $idx = count($faculty->getDepartments());
    $faculty->getDepartments()[] = $department;
} else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $idx = $_GET['idx'];
    $department = $faculty->getDepartments()[$idx];
}

$employees = $department->getEmployees();
$subjects = $department->getSubjects();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Přidat fakultu</title>
    <link href="/public/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
</head>
<body>
<main class="container">
    <?php require_once '../../../templates/navbar.php'; ?>

    <h4><a href="/new/faculty">⬅️ Fakulta</a></h4>

    <br/>

    <?php include_once '../../../templates/department.php'; ?>

    <hr/>

    <section class="row">
        <article class="col-6">
            <h4>Zaměstnanci</h4>
            <section>
                <?php foreach($employees as $i=> $employee) { ?>
                    <?= ($i != 0) ? '<hr/>' : '' ?>
                    <article class="row">
                        <label class="form-label">Jméno: <?=$employee->getName()?></label>
                        <label class="form-label">Telefon: <?=$employee->getPhone()?></label>
                        <label class="form-label">Email: <?=$employee->getEmail()?></label>
                    </article>
                <?php } ?>
            </section>
        </article>
        <article class="col-6">
            <h4>Předmety</h4>
            <section>
                <?php foreach($subjects as $i=> $subject) { ?>
                    <?= ($i != 0) ? '<hr/>' : '' ?>
                    <article class="row">
                        <label class="form-label"><?=$subject->getShortcut()?> - <?=$subject->getName()?></label>
                        <label class="form-label">Typ: <?=$subject->getType()?></label>
                        <label>Popis:</label>
                        <p><?=$subject->getDescription()?></p>
                    </article>
                <?php } ?>
            </section>
        </article>
    </section>

    <hr/>

    <h2>Noví Zaměstnanec:</h2>

    <form class="col" enctype="multipart/form-data" method="POST" action="/new/department/employee/">
        <section class="row">
            <article class="col-6 col-md-4">
                <label for="employee-name" class="form-label">Jméno</label>
                <input class="form-control mb-3" type="text" name="name" id="employee-name" required>
            </article>
            <article class="col-6 col-md-3">
                <label for="employee-phone" class="form-label">Telefon</label>
                <input class="form-control mb-3" type="tel" name="phone" id="employee-phone" required>
            </article>
            <article class="col-12 col-md-5">
                <label for="employee-email" class="form-label">Email</label>
                <input class="form-control mb-3" type="email" name="email" id="employee-email" required>
            </article>
            <input style="display: none" name="idx" value="<?=$idx?>" />
        </section>

        <button class="form-control btn btn-outline-primary mb-3" type="submit">Přidat zaměstnance</button>
    </form>

    <hr/>

    <h2>Noví Předmět:</h2>

    <form class="col" enctype="multipart/form-data" method="POST" action="/new/department/subject/">
        <section class="row">
            <article class="col-4 col-md-2">
                <label for="subject-shortcut" class="form-label">Zkratka</label>
                <input class="form-control mb-1" type="text" name="shortcut" id="subject-shortcut" required>
            </article>
            <article class="col-8 col-md-6">
                <label for="subject-name" class="form-label">Název</label>
                <input class="form-control mb-5" type="tel" name="name" id="subject-name" required>
            </article>
            <article class="col-12 col-md-4">
                <label for="subject-type" class="form-label">Typ</label>
                <select class="form-select" name="type" id="subject-type" required>
                    <option value="přednáška" selected>Přednáška</option>
                    <option value="seminář">Seminář</option>
                    <option value="cvičení">Cvičení</option>
                    <option value="kombinované">Kombinované</option>
                </select>
            </article>
            <article class="col-12">
                <div class="form-floating">
                    <textarea class="form-control" id="subject-description" name="description" style="height: 100px"></textarea>
                    <label for="subject-description">Popis</label>
                </div>
            </article>
            <input style="display: none" name="idx" value="<?=$idx?>" />
        </section>

        <br/>

        <button class="form-control btn btn-outline-primary mb-3" type="submit">Přidat předmět</button>
    </form>
</main>

<script src="/public/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</body>
</html>


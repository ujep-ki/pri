<?php
    include_once '/var/www/objects/import.php';

    session_start();

    $idx = $_POST['idx'];

    /** @var Faculty $faculty */
    $faculty = $_SESSION['new'];

    /** @var Department $department */
    $department = $faculty->getDepartments()[$idx];

    $department->getEmployees()[] = Employee::fromPost($_POST);

    header("Location: /new/department/?idx=$idx");
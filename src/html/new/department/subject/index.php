<?php
    include_once '/var/www/objects/import.php';

    session_start();

    $idx = $_POST['idx'];

    /** @var Faculty $faculty */
    $faculty = $_SESSION['new'];

    /** @var Department $department */
    $department = $faculty->getDepartments()[$idx];

    $subject = Subject::fromPost($_POST);

    $department->getSubjects()[] = $subject;

    header("Location: /new/department/?idx=$idx");
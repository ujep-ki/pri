<?php
    include_once '/var/www/objects/faculty.php';
    include_once '/var/www/db/connection.php';

    $xmlFilePath = $_FILES['fakulta']['tmp_name'];
    $rawXml = file_get_contents($xmlFilePath);
    unlink($xmlFilePath);

    $dom = new DOMDocument;
    $dom->loadXML($rawXml);

    if ($dom->schemaValidate('../../../templates/template.xsd')){
        $simpleXmlDom = simplexml_load_string($rawXml);
        $xmlFaculty = $simpleXmlDom->xpath('/fakulta')[0];

        print_r($xmlFaculty);
        $faculty = Faculty::fromXml($xmlFaculty);

        $faculty->saveToDB($dbConnection);

        header('Location: /');
    } else {
        echo "ERROR";
    }




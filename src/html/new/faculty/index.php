<?php
    include '/var/www/objects/faculty.php';

    session_start();

    if(!isset($_SESSION['new']) && $_SERVER['REQUEST_METHOD'] !== 'POST') {
        header('Location: /new');
    }

    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $_SESSION['new'] = Faculty::fromPostData($_POST);
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Přidat fakultu</title>
    <link href="/public/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
</head>
<body>
    <main class="container">
        <?php require_once '../../../templates/navbar.php'; ?>

        <?php require_once '../../../templates/faculty.php'; ?>

        <section>
            <?php foreach($_SESSION['new']->getDepartments() as $idx=>$department) require '../../../templates/department.php'; ?>
        </section>

        <hr/>

        <form class="col" enctype="multipart/form-data" method="POST" action="/new/department/">
            <h2>Nová Katedra:</h2>

            <section class="row">
                <article class="col-12 col-sm-4">
                    <label for="shortcut" class="form-label">Zkratka</label>
                    <input class="form-control mb-3" type="text" name="shortcut" id="shortcut" required>
                </article>
                <article class="col-12 col-sm-8">
                    <label for="name" class="form-label">Název</label>
                    <input class="form-control mb-3" type="text" name="name" id="name" required>
                </article>
            </section>

            <h4>Vedoucí katedry:</h4>

            <section class="row">
                <article class="col-6 col-md-3">
                    <label for="supervisor-name" class="form-label">Jméno</label>
                    <input class="form-control mb-3" type="text" name="supervisor-name" id="supervisor-name" required>
                </article>
                <article class="col-6 col-md-3">
                    <label for="supervisor-phone" class="form-label">Telefon</label>
                    <input class="form-control mb-3" type="tel" name="supervisor-phone" id="supervisor-phone" required>
                </article>
                <article class="col-12 col-md-6">
                    <label for="supervisor-email" class="form-label">Email</label>
                    <input class="form-control mb-3" type="email" name="supervisor-email" id="supervisor-email" required>
                </article>
            </section>

            <button class="form-control btn btn-outline-primary mb-3" type="submit">Přidat katedru</button>
        </form>

        <hr/>

        <a href="/new/create" class="form-control btn btn-success mb-3">Uložit Fakultu</a>
    </main>

    <script src="/public/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</body>
</html>

<?php
include '/var/www/objects/faculty.php';
include '/var/www/db/connection.php';

session_start();

if(isset($_SESSION['new'])) {
    $facultyId = $_SESSION['new']->saveToDB($dbConnection);

    unset($_SESSION['new']);

    header("Location: /item/?id=$facultyId");
}
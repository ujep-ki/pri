<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fakultonahrávač</title>
    <link href="/public/bootstrap.min.css" rel="stylesheet" />
</head>

<body>
<main class="container">
    <?php include_once '../../templates/navbar.php'; ?>

    <div class="card text-center">
        <h1 class="card-body">Přidat fakultu</h1>
    </div>

    <br />

    <section class="row row-cols-1 row-cols-sm-1 row-cols-md-2">
        <form class="col" method="POST" action="faculty/">
            <h2>Zadat ručně</h2>

            <label for="faculty-name" class="form-label">Název fakulty</label>
            <input class="form-control mb-3" type="text" name="faculty-name" id="faculty-name" required>

            <label for="dean-name" class="form-label">Děkan fakulty</label>
            <input class="form-control mb-3" type="text" name="dean-name" id="dean-name" required>

            <button class="form-control btn btn-outline-success mb-3" type="submit">Vytvořit Fakultu Ručně</button>
        </form>
        <form class="col" enctype="multipart/form-data" method="POST" action="file/">
            <h2>Nahrát pomocí souboru</h2>
            <label for="file" class="form-label">Soubor fakulty</label>
            <input class="form-control mb-3" type="file" name="fakulta" id="file" accept="application/xml" required>
            <button class="form-control btn btn-outline-success mb-3" type="submit">Odeslat</button>
        </form>
    </section>
</main>

    <script src="/public/bootstrap.bundle.min.js"></script>
</body>

</html>
<?php
    global $dbConnection;
    include_once '/var/www/db/connection.php';
    include_once '/var/www/objects/faculty.php';

    $facultyId = $_GET['id'];
    $faculty = Faculty::getOneByIdDB($dbConnection, $facultyId);

    $xslTemplate = simplexml_load_file('/var/www/templates/template.xsl');
    $facultyXml = $faculty->toXml();

    $xslProcessor = new XSLTProcessor();
    $xslProcessor->importStylesheet($xslTemplate);
    $transformedHtml = $xslProcessor->transformToXml($facultyXml);
?>
<html lang="cs">
<head>
    <link href="/public/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous"/>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <title><?= $faculty->getName()?> Fakulta</title>
</head>
<body>

<main class="container">
    <?php include_once '../../templates/navbar.php'; ?>

    <article class="card text-center">
        <h1 class="card-body"><?= $faculty->getName()?> Fakulta (<a href="/item/xml/?id=<?=$facultyId?>">XML</a>)</h1>
        <h4><b>Děkan:</b> <?=$faculty->getDean()?></h4>
    </article>

    <br />

    <article><?= $transformedHtml ?></article>

    <script src="/public/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</main>
</body>
</html>
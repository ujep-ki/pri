<?php
    global $dbConnection;
    include_once '/var/www/db/connection.php';
    include_once '/var/www/objects/faculty.php';

    $id = $_GET['id'];

    $faculty = Faculty::getOneByIdDB($dbConnection, $id);
    $facultyName = preg_replace('/\s+/', '_', $faculty->getName());

    header('Content-Type: application/xml');
    header("Content-Disposition: attachment; filename=\"$id-$facultyName.xml\"");

    echo $faculty->toXml()->asXML();
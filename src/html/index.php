<?php
    include_once '../db/connection.php';

    $files = mysqli_fetch_all(mysqli_query($dbConnection, "SELECT id, name FROM faculties;"), MYSQLI_ASSOC);

//    $filesDir = $_SERVER['DOCUMENT_ROOT'] . '/../data/files';
//    $files = array_diff(scandir($filesDir), array('..', '.'));
?>

<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fakultonahrávač</title>
    <link href="/public/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
</head>
<body>
    <main class="container">
        <?php include_once '../templates/navbar.php'; ?>

        <article class="row text-center">
            <?php foreach($files as $file) include '../templates/file-button.php'; ?>
        </article>
    </main>

    <script src="/public/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</body>
</html>
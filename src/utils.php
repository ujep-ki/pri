<?php

class Utils
{
    public static function xml_attribute(SimpleXMLElement $object, $attribute)
    {
        if(isset($object[$attribute]))
            return (string) $object[$attribute];
    }
}
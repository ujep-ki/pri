<?php
global $dbConnection;
$dbConnection = mysqli_connect("database", "pri", "pri", "pri");

$tablesFound = mysqli_fetch_all(mysqli_query( $dbConnection, "SHOW tables;"));
$tablesLoaded = false;

foreach ($tablesFound as $row) {
    if($row[0] === "loaded") {
        $tablesLoaded = true;
        break;
    }
}

if(!$tablesLoaded) {
    mysqli_multi_query($dbConnection, file_get_contents("/var/www/db/init.sql"));
    mysqli_query($dbConnection, "CREATE TABLE loaded(loaded bool not null);");
}

//$result = mysqli_query($dbConnection, "SELECT 'Welcome to MySQLi' AS _msg FROM DUAL");
//$row = mysqli_fetch_assoc($result);
//echo $row['_msg'];
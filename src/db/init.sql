CREATE TABLE `faculties` (
                             `id` INT (10) NOT NULL AUTO_INCREMENT,
                             `name` VARCHAR (255) NOT NULL,
                             `dean_name` VARCHAR (255) NOT NULL,
                             PRIMARY KEY (`id`)
)
    ENGINE=InnoDB
    DEFAULT CHARSET=utf8;


CREATE TABLE `persons` (
                           `id` INT (10) NOT NULL AUTO_INCREMENT,
                           `name` VARCHAR (255) NOT NULL,
                           `phone` VARCHAR (255) NOT NULL,
                           `email` VARCHAR (255) NOT NULL,
                           PRIMARY KEY (`id`)
)
    ENGINE=InnoDB
    DEFAULT CHARSET=utf8;


CREATE TABLE `departments` (
                               `id` INT (10) NOT NULL AUTO_INCREMENT,
                               `name` VARCHAR (255) NOT NULL,
                               `shortcut` VARCHAR (5) NOT NULL,
                               `faculty_id` INT (10) NOT NULL,
                               `supervisor_id` INT (10) NOT NULL,
                               PRIMARY KEY (`id`)
)
    ENGINE=InnoDB
    DEFAULT CHARSET=utf8;


CREATE TABLE `employees` (
                             `department_id` INT (10) NOT NULL,
                             `person_id` INT (10) NOT NULL,
                             PRIMARY KEY (`department_id`, `person_id`)
)
    ENGINE=InnoDB
    DEFAULT CHARSET=utf8;


CREATE TABLE `subjects` (
                            `id` INT (10) NOT NULL AUTO_INCREMENT,
                            `name` VARCHAR (255) NOT NULL,
                            `shortcut` VARCHAR (255) NOT NULL,
                            `type` VARCHAR (255) NOT NULL,
                            `description` TEXT NOT NULL,
                            `department_id` INT (10) NOT NULL,
                            PRIMARY KEY (`id`)
)
    ENGINE=InnoDB
    DEFAULT CHARSET=utf8;


ALTER TABLE `departments`
    ADD FOREIGN KEY faculties_departments_fk (`faculty_id`) REFERENCES `faculties`(`id`);

ALTER TABLE `employees`
    ADD FOREIGN KEY departments_employees_fk (`department_id`) REFERENCES `departments`(`id`);

ALTER TABLE `employees`
    ADD FOREIGN KEY persons_employees_fk (`person_id`) REFERENCES `persons`(`id`);

ALTER TABLE `subjects`
    ADD FOREIGN KEY departments_subjects_fk (`department_id`) REFERENCES `departments`(`id`);

ALTER TABLE `departments`
    ADD FOREIGN KEY persons_departments_fk (`supervisor_id`) REFERENCES `persons`(`id`);